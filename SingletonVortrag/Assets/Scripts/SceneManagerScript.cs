﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SceneManagerScript : MonoBehaviour
{
    public Text countText;

    private void Start()
    {
        countText.text = PersistentManager.Instance.count.ToString();
    }

    public void GoToFirstScene()
    {
        SceneManager.LoadScene("first");
        PersistentManager.Instance.count++;

    }

    public void GoToSecondScene()
    {
        SceneManager.LoadScene("second");
        PersistentManager.Instance.count++;

    }

}
